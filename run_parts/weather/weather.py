import os

from vaska.config import db_str as connection_tring
from run_parts.weather.src import picker
from run_parts.weather.src import text


def get_weather(**config):
    print(os.getcwd())
    places = tuple(config['weather_coordinates'].keys())
    coordinates = tuple(config['weather_coordinates'].values())
    message = prepare_message(connection_tring, places, coordinates)
    return message


def prepare_message(connection_string, places: tuple, coordinates: tuple):
    current_place = places[0]
    current_weather = picker.get_weather(*coordinates[0])

    alt_place = places[1]
    alt_weather = picker.get_weather(*coordinates[1])

    wish = picker.get_sketch('Пожелание', connection_string)
    smile = picker.get_sketch('Смайлик', connection_string)
    greeting = picker.get_sketch('Приветствие', connection_string)

    message = text.create_message(
        current_place, current_weather, alt_place, alt_weather,
        wish, greeting, smile
    )
    return message
