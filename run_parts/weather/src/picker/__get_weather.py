__all__ = ['get_weather']

import json
import requests
from datetime import datetime


def get_weather(lat, lon):
    return __parser(lat, lon)


def __parser(lat, lon):
    codes = {
        0: 'ясно',
        1: 'преимущественно ясно, переменная облачность',
        2: 'преимущественно ясно, переменная облачность',
        3: 'переменная облачность',
        45: 'туман',
        48: 'изморозь',
        51: 'легкая морось',
        53: 'умеренная морось',
        55: 'плотная морось',
        56: 'легкая ледяная морось',
        57: 'плотная ледяная морось',
        61: 'слабый дождь',
        63: 'умеренный дождь',
        65: 'сильный дождь',
        66: 'легкий ледяной дождь',
        67: 'сильный ледяной дождь',
        71: 'легкий снегопад',
        73: 'умеренный снегопад',
        75: 'сильный снегопад',
        77: 'мокрый снег',
        80: 'легкий ливень',
        81: 'умеренный ливень',
        82: 'сильный ливень',
        85: 'слабый снег',
        86: 'сильный снег',
        95: 'гроза',
        96: 'гроза с градом',
        99: 'гроза с сильным градом',
    }

    response = __weather_from_api(lat, lon)
    return codes[response['hourly']['weathercode'][6]], \
        response['hourly']['temperature_2m'][6], \
        codes[response['daily']['weathercode'][0]], \
        response['daily']['temperature_2m_min'][0], \
        response['daily']['temperature_2m_max'][0]


def __weather_from_api(lat, lon):
    today = datetime.now().date()
    api_url = f'https://api.open-meteo.com/v1/forecast?latitude={lat}&' \
              f'longitude={lon}&timezone=auto&start_date={today}&' \
              f'end_date={today}&daily=temperature_2m_max,temperature_2m_min' \
              f',weathercode&hourly=weathercode,temperature_2m'

    response = requests.get(api_url)
    response = json.loads(response.content)
    return response
