__all__ = ['get_sketch']
from vaska.db.db_api import request_execute
import sqlalchemy
import psycopg2


def get_sketch(sketch_type, conn_str):
    return request_sketches(sketch_type, conn_str)


def request_sketches(type_name, conn_str):
    text = f"""
        select a.sketch from bot.sketches a
        join bot.sketch_types b on a.type = b.id and b.type_name = '{type_name}'
        where a.is_active = 1
        order by random() 
        limit 1
    """
    cursor = request_execute(text).get('response')

    for i in cursor:
        return i[0]