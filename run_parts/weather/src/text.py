def create_message(
        current_place, current_weather, alt_place, alt_weather, wish, greeting, smile
):
    current = f"{current_place} сейчас {current_weather[0]}, " \
              f"температура воздуха около {current_weather[1]}."

    alternative_weather_string = \
        alt_weather[0] + ', ' + str(alt_weather[1]) if alt_weather[1] != current_weather[1] else 'ситуация похожая'

    alternative = f"{alt_place} {alternative_weather_string}."

    predict = f"Днем ожидается {current_weather[2]}, воздух " \
              f"{current_weather[3]}  {current_weather[4]}"

    message = greeting + "\n\n" + current + "\n" + alternative + "\n\n" + predict + "\n\n" + wish + " " + smile

    return message
