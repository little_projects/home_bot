from sys import argv
from vaska.config import bot
from vaska.runner import create_message
import asyncio
from time import sleep
from random import randint


async def run_part(part):
    chat_id, message_text = create_message(part)
    seanse = await bot.get_session()
    await bot.send_message(chat_id, message_text)
    await seanse.close()


if __name__ == '__main__':
    sleep_time = randint(10, 20)
    sleep(sleep_time)
    runner_part = argv[1]
    asyncio.run(run_part(runner_part))
