from aiogram import executor, Dispatcher
from vaska.config import Config
from vaska.handlers import dp
from vaska.input import *
# from vaska import midlewares
from vaska.db.db_api import test_connect
from datetime import datetime

config = Config()


def log_record(message, is_errore=0):
    rec = f"{datetime.now()} [{'ERROR' if is_errore==1 else 'INFO'}]: {message}\n"
    with open('vaska.log', 'a', encoding='utf8') as file:
        file.write(rec)


async def on_startup(dispatcher: Dispatcher):
    print('run, baby!')
    msg = f'Бот...  ОК\nБаза... {test_connect()}'
    admins = config.get_parts(['admins'])

    if admins is None:
        print('Никаких админов не существует')
        print(msg)
        return

    for i in admins:
        await dispatcher.bot.send_message(i, msg)


if __name__ == '__main__':
    # midlewares.mw_setup(dp)
    params = config.get_parts(['init'])
    relax = params['relax']
    timeout = params['timeout']
    try:
        log_record('Бот запущен')
        executor.start_polling(dp, on_startup=on_startup, timeout=timeout, relax=relax)
    except:
        log_record('Возникла ошибка!', 1)

