__all__ = ['dp']

from ..config import Config
from aiogram.types import Message
from ..db import log

config = Config()
dp = config.dispatcher

# help_msg = """
# Привет!
#
# Я умею отслеживать вес: /weight_help
# Подсказывать простые числа: /get_simple
# Рассчитывать размер выборок: /calc_size
# """

help_msg = """
Привет!

Я пока ничего не умею )
"""

#
# @dp.message_handler(commands=['start', 'help'])
# async def start(message: Message):
#     log.user_start_action(message.from_id)
#     await message.answer(help_msg)
#
#
# @dp.message_handler(commands=['help'])
# async def start(message: Message):
#     await message.answer(help_msg)


@dp.message_handler()
async def total(message: Message):
    if message.chat['id'] > 0:
        await message.answer(f"{message.from_user['first_name']}, я пока ничего не умею 🤷‍")
    log.log_record(message)
