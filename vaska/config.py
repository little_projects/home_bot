__all__ = ['Config']

import yaml
from dotenv import dotenv_values
from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage


class ConfigStructure:
    def __init__(self):
        self.path_to_config = dotenv_values('.env').get('config_file', 'config.yml')
        self.storage_type = MemoryStorage()
        self.config = self.set_config()
        self.bot = Bot(self.get_parts(['init', 'token']))
        self.dispatcher = Dispatcher(self.bot, storage=self.storage_type)
        self.db_str = self.get_parts(['init', 'db_str'])

    def set_config(self):
        with open(self.path_to_config, 'r') as file:
            config = yaml.safe_load(file)
        return config

    def get_parts(self, part_path: list, setting=None):
        result = self.config
        for part in part_path:
            result = result.get(part, None)
            if result is None:
                break
        if setting is None:
            return result
        else:
            setting = result


class Config(ConfigStructure):
    instance = None

    def __new__(cls, *args, **kwargs):
        if Config.instance is None:
            Config.instance = super().__new__(cls)
        return Config.instance
