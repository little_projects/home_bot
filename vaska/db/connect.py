__all__ = ['req_execute']

import sqlalchemy
import psycopg2
from vaska.config import Config

config = Config()
db_str = config.db_str


def create_connect(conn_str):
    engine = sqlalchemy.create_engine(conn_str)
    return engine


def run(req, conn_str, commit):
    if conn_str is None:
        engine = create_connect(db_str)
    else:
        engine = create_connect(conn_str)
    with engine.connect() as connect:
        result = connect.execute(req)
        if commit:
            connect.commit()
    return {'status': 'OK', 'response': result}


def req_execute(req: str, conn_str=None, commit=False):
    req = sqlalchemy.text(req)
    try:
        return run(req, conn_str, commit)
    except:
        return {'status': 'FAIL', 'response': None}
