__all__ = ['test_connect', 'request_execute']

from . import connect


def test_connect():
    test_req = """
        select current_database()
    """
    return request_execute(test_req).get('status', None)


def request_execute(req, conn_str=None, commit=False):
    return connect.req_execute(req, conn_str, commit)
