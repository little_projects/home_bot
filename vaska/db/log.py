from . import db_api
from datetime import datetime
from ..config import Config


def table_name():
    config = Config().get_parts(['init'])
    return config['log_table']


def log_record(main_data):
    req = f"""
        insert into {table_name()} (rec_time, message, bot_name)
        values ('{datetime.now()}', '{main_data}', 'vaska')
    """
    db_api.request_execute(req, commit=True)
